require('./models/mongodb')
const countryController= require('./controllers/countryController')
const provinceController= require('./controllers/provinceController')
const districtController = require('./controllers/districtController')
const sectorController = require('./controllers/sectorController')
const express = require('express')

var app= express()

const bodyparser = require('body-parser');
 
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

app.get('/',(req,res)=>{
    res.send('Welcome To The National Administrative Divisions Programm')
})

app.use('/api/countries',countryController)
app.use('/api/provinces',provinceController)
app.use('/api/districts',districtController)
app.use('/api/sectors',sectorController)

const port = process.env.PORT || 5000

app.listen(port,()=>console.log(`server is running on port ${port}....`))
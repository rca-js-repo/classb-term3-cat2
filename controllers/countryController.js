const express = require('express')
const mongoose = require('mongoose')

var router = express.Router();

const Country = mongoose.model('Country')

router.post('/',(req,res)=>{
    insertIntoMongoDB(req,res)
})

router.put('/',(req,res)=>{
    updateIntoMongoDB(req,res)
})

function insertIntoMongoDB(req,res){
    let country= new Country
    country.countryName = req.body.countryName
    country. countryContinent = req.body.countryContinent

    country.save()
    .then(countrySaved=>res.send(countrySaved).status(201))
    .catch(err=>res.send(err).status(404))
}

function updateIntoMongoDB(req,res){
    Country.findByIdAndUpdate({_id:req.body._id},req.body,{new: true})
    .then(countrySaved=>res.send(countrySaved).status(201))
    .catch(err=>res.send(err).status(404))
}

router.get('/',(req,res)=>{
    Country.find()
    .then(countrySaved=>res.send(countrySaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.get('/:id',(req,res)=>{
    Country.find({_id:req.params.id})
    .then(countrySaved=>res.send(countrySaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.delete('/:id',(req,res)=>{
    Country.findByIdAndDelete(req.params.id)
    .then(countrySaved=>res.send(countrySaved).status(201))
    .catch(err=>res.send(err).status(404))
})

module.exports = router

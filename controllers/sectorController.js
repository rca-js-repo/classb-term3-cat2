const express = require('express')
const mongoose = require('mongoose')

var router = express.Router();
const Sector = mongoose.model('Sector')
const District = mongoose.model('District')

router.post('/',(req,res)=>{
    insertIntoMongoDB(req,res)
})

router.put('/',(req,res)=>{
    updateIntoMongoDB(req,res)
})

function insertIntoMongoDB(req,res){
    let sector= new Sector
    sector.sectorName = req.body.sectorName
    District.find({_id:req.body.districtId})
    if(!District)
        
        return res.send('No district with that id').status(404)
        sector.districtId = req.body.districtId

    sector.save()
    .then(sectorSaved=>res.send(sectorSaved).status(201))
    .catch(err=>res.send(err).status(404))
}

function updateIntoMongoDB(req,res){
    Sector.findByIdAndUpdate({_id:req.body._id},req.body,{new: true})
    .then(sectorSaved=>res.send(sectorSaved).status(201))
    .catch(err=>res.send(err).status(404))
}

router.get('/',(req,res)=>{
    Sector.find()
    .then(sectorSaved=>res.send(sectorSaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.get('/:id',(req,res)=>{
    Sector.find({_id:req.params.id})
    .then(sectorSaved=>res.send(sectorSaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.delete('/:id',(req,res)=>{
    Sector.findByIdAndDelete(req.params.id)
    .then(sectorSaved=>res.send(sectorSaved).status(201))
    .catch(err=>res.send(err).status(404))
})
module.exports = router
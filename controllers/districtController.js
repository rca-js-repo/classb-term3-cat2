const express = require('express')
const mongoose = require('mongoose')

var router = express.Router();
const Province = mongoose.model('Province')
const District = mongoose.model('District')

router.post('/',(req,res)=>{
    insertIntoMongoDB(req,res)
})

router.put('/',(req,res)=>{
    updateIntoMongoDB(req,res)
})

function insertIntoMongoDB(req,res){
    let district= new District
    district.districtName = req.body.districtName
    Province.find({_id:req.body.provinceId})
    if(!Province)
        
        return res.send('No province with that id').status(404)
        district.provinceId = req.body.provinceId
    
    

    district.save()
    .then(districtSaved=>res.send(districtSaved).status(201))
    .catch(err=>res.send(err).status(404))
}

function updateIntoMongoDB(req,res){
    District.findByIdAndUpdate({_id:req.body._id},req.body,{new: true})
    .then(districtSaved=>res.send(districtSaved).status(201))
    .catch(err=>res.send(err).status(404))
}

router.get('/',(req,res)=>{
    District.find()
    .then(districtSaved=>res.send(districtSaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.get('/:id',(req,res)=>{
    District.find({_id:req.params.id})
    .then(districtSaved=>res.send(districtSaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.delete('/:id',(req,res)=>{
    District.findByIdAndDelete(req.params.id)
    .then(districtSaved=>res.send(districtSaved).status(201))
    .catch(err=>res.send(err).status(404))
})
module.exports = router
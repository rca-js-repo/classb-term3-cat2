const express = require('express')
const mongoose = require('mongoose')

var router = express.Router();
const Country = mongoose.model('Country')
const Province = mongoose.model('Province')

router.post('/',(req,res)=>{
    insertIntoMongoDB(req,res)
})

router.put('/',(req,res)=>{
    updateIntoMongoDB(req,res)
})

function insertIntoMongoDB(req,res){
    let province= new Province
    province.provinceName = req.body.provinceName
    Country.find({_id:req.body.countryId})
    if(!Country)
        
        return res.send('No country with that id').status(404)
        province.countryId = req.body.countryId
    
    

    province.save()
    .then(provinceSaved=>res.send(provinceSaved).status(201))
    .catch(err=>res.send(err).status(404))
}

function updateIntoMongoDB(req,res){
    Province.findByIdAndUpdate({_id:req.body._id},req.body,{new: true})
    .then(provinceSaved=>res.send(provinceSaved).status(201))
    .catch(err=>res.send(err).status(404))
}

router.get('/',(req,res)=>{
    Province.find()
    .then(provinceSaved=>res.send(provinceSaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.get('/:id',(req,res)=>{
    Province.find({_id:req.params.id})
    .then(provinceSaved=>res.send(provinceSaved).status(201))
    .catch(err=>res.send(err).status(404))
})

router.delete('/:id',(req,res)=>{
    Province.findByIdAndDelete(req.params.id)
    .then(provinceSaved=>res.send(provinceSaved).status(201))
    .catch(err=>res.send(err).status(404))
})


module.exports = router
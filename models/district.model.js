const mongoose = require('mongoose')

var districtSchema = new mongoose.Schema({
    districtName:{
        type: String,
        required: 'This field is required',
        min:10,
        max:100
    },
    provinceId:{
        type: String,
        required: 'This field is required',
        min:10,
        max: 100
    }

})

mongoose.model('District',districtSchema)
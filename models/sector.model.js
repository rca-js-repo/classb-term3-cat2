const mongoose = require('mongoose')

var sectorSchema = new mongoose.Schema({
    sectorName:{
        type: String,
        required: 'This field is required',
        min:10,
        max:100
    },
    districtId:{
        type: String,
        required: 'This field is required',
        min:10,
        max: 100
    }

})

mongoose.model('Sector',sectorSchema)
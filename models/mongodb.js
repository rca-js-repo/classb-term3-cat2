const mongoose = require('mongoose')

    mongoose.connect('mongodb://localhost/national-administrative-divisions',{
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
.then(()=>console.log('Connected to mongodb successfully....'))

.catch(err=>console.log('failed to connect to mongodb...',err));

require('./country.model')
require('./province.model')
require('./district.model')
require('./sector.model')
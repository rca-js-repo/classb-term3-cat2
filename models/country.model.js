const mongoose = require('mongoose')

var countrySchema = new mongoose.Schema({
    countryName:{
        type: String,
        required: 'This field is required',
        min:10,
        max:100
    },
    countryContinent:{
        type: String,
        required: 'This field is required',
        min:10,
        max: 100
    }

})

mongoose.model('Country',countrySchema)
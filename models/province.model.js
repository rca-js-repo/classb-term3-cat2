const mongoose = require('mongoose')

var provinceSchema = new mongoose.Schema({
    provinceName:{
        type: String,
        required: 'This field is required',
        min:10,
        max:100
    },
    countryId:{
        type: String,
        required: 'This field is required',
        min:10,
        max: 100
    }

})

mongoose.model('Province',provinceSchema)